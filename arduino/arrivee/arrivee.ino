// Réalisé à l'aide des exemples fournis avec la bibliotheque NRFLite.h publiée sous licence MIT par Dave Parson
#include <SPI.h>
#include <NRFLite.h>

const static uint8_t RADIO_ID = 1;
const static uint8_t PIN_RADIO_CE = 9;
const static uint8_t PIN_RADIO_CSN = 10;

const static uint8_t PIN_RPI = 4;

struct RadioPacket // structure pour les paquets
{
    uint8_t FromRadioId;
    boolean portiqueOuvert;
    uint32_t FailedTxCount;
};

NRFLite _radio;

RadioPacket _radioData;

void setup()
{
    Serial.begin(9600);

    // Initialisation de la radio
    if (!_radio.init(RADIO_ID, PIN_RADIO_CE, PIN_RADIO_CSN))
    {
        Serial.println("Cannot communicate with radio");
        while (1);
    }
    // On force l'initialisation à "portique fermé" 
    _radioData.portiqueOuvert = 0;

    // On initialise la pin de sortie pour le RPI
    pinMode(PIN_RPI, OUTPUT);
    digitalWrite(PIN_RPI, 0);

}

void loop()
{
  while (_radio.hasData())
  {
    _radio.readData(&_radioData);
    Serial.println(_radioData.portiqueOuvert);
  }
}
