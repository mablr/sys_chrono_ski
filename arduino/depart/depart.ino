// Réalisé à l'aide des exemples fournis avec la bibliotheque NRFLite.h publiée sous licence MIT par Dave Parson
#include <SPI.h>
#include <NRFLite.h>

const static uint8_t RADIO_ID = 0;
const static uint8_t DESTINATION_RADIO_ID = 1; 
const static uint8_t PIN_RADIO_CE = 9;
const static uint8_t PIN_RADIO_CSN = 10;

const static uint8_t PIN_MICRO_RUPT = 8;


struct RadioPacket // structure pour les paquets
{
    uint8_t FromRadioId;
    boolean portiqueOuvert;
    uint32_t FailedTxCount;
};

// var radio
NRFLite _radio(Serial);

// var pour stocker le contenus des messages radio
RadioPacket _radioData;



void setup()
{
    Serial.begin(9600);

    /*
     *  _radio.printChannels();
     *  Permet de scanner les canaux libres
     *  Par défaut on utilise le 100
     */

    // Initialisation de la radio
    if (!_radio.init(RADIO_ID, PIN_RADIO_CE, PIN_RADIO_CSN, NRFLite::BITRATE2MBPS, 100))
    {
        Serial.println("Cannot communicate with radio");
        // Si l'initialisation échoue on ne va pas plus loin
        while (1);
    }
    // Initialisation de l'id de la radio
    _radioData.FromRadioId = RADIO_ID;

    // On init le pin sur lequel le microrupteur est branché en entrée
    pinMode(PIN_MICRO_RUPT,INPUT);

}

void loop() {
  // put your main code here, to run repeatedly:
  Serial.println(digitalRead(PIN_MICRO_RUPT));
  _radioData.portiqueOuvert = digitalRead(PIN_MICRO_RUPT);
  _radio.send(DESTINATION_RADIO_ID, &_radioData, sizeof(_radioData), NRFLite::NO_ACK);
}
