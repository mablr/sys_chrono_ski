#!/usr/bin/python3
# (c) Mablr
# GNU GPL v3


import time
import cv2
import serial

avertPortique = False
portiqueOuvert = True
seqInit = True
courseEnCours = False

cap = cv2.VideoCapture(0)
fgbg = cv2.createBackgroundSubtractorMOG2()
detectShadows = True

fermeturePropre = 0
ouverturePropre = 0

# Seuil de détection
seuilDetec = 5000

# Peut etre a enlever
continuer = True

# Initialisation de la connexion série
ser = serial.Serial('/dev/ttyACM0',9600)

while continuer:
    # Récupération de l'état du portique
    serialDATA = str(ser.readline())[2:3]

    if serialDATA == "0":
        portiqueOuvert = False
    else:
        portiqueOuvert = True

    ## Séquence d'initialisation
    if portiqueOuvert and seqInit and not courseEnCours:
        fermeturePropre = 0# remise à zéro du compteur fermeture stable
        if not avertPortique:
            print("Veuillez fermer le portique.")
            avertPortique = True

    # Si le portique est fermé
    if not portiqueOuvert and seqInit and not courseEnCours:
        time.sleep(0.01)
        fermeturePropre += 1 # incrément fermeture stable

    if not portiqueOuvert and fermeturePropre > 20 and not courseEnCours and seqInit:
        # On peut résonablement penser que le portique de départ est bien fermé
        input("Taper sur ENTRER pour autoriser le départ, ou Ctrl+C pour quitter le programme.\n")
        seqInit = False
        avertPortique = False
        ouverturePropre = 0

    # Si le portique ouvre (départ du skieur)
    if portiqueOuvert and not seqInit and not courseEnCours:
        ouverturePropre += 1 # incrément ouverture stable

    ## Début de la course
    if portiqueOuvert and ouverturePropre > 100 and not seqInit and not courseEnCours:
        tsDepart = time.time()
        courseEnCours = True
        print("Timestamp de départ :", tsDepart)

        # Stabilisation de l'image
        for i in range(100):
            ret, frame = cap.read()
            fgmask = fgbg.apply(frame)


    if courseEnCours and not seqInit:
        ret, frame = cap.read()
        fgmask = fgbg.apply(frame)
        countWhite = cv2.countNonZero(fgmask)
        # Si on détecte un skieur
        if countWhite >= seuilDetec:
            # Traitement du temps du skieur
            tsArrivee = time.time()
            temps = tsArrivee - tsDepart
            print("\n#################################\n# Temps de course:", str(temps)[:7],"sec. #\n#################################\n")
            # La course est finie
            courseEnCours = False
            seqInit = True

