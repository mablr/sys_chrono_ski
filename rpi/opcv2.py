#!/usr/bin/python3
# Code inspiré par la doc d'OpenCV https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_video/py_bg_subtraction/py_bg_subtraction.html#backgroundsubtractormog2

import numpy as np
import cv2

cap = cv2.VideoCapture(0)
fgbg = cv2.createBackgroundSubtractorMOG2()
detectShadows = False
while(1):
    ret, frame = cap.read()

    fgmask = fgbg.apply(frame)
    count_white = cv2.countNonZero(fgmask)
    print(count_white)
    cv2.imshow('frame',fgmask)
    k = cv2.waitKey(30) & 0xff
    if k == 27:
        break

cap.release()
cv2.destroyAllWindows()
