# Système de chronométrage de course (de ski) par analyse vidéo

Le système est basé sur 2 Arduinos avec un module nRF24 chacun et 1 Raspberry Pi une Pi cam.

Le code Python est assez sommaire, n'hésitez pas à me faire remonter vos suggestions.

Les informations qui suivent permettent de re-déployer le chrono facilement. Des pistes d'amélioration intéressantes sont également proposées.

# Introduction
Le système qui va être décrit par la suite, permet de proposer une solution de chronométrage à moindre coût pour tous types de disciplines sportives.
La partie la plus intéressante est la détection de franchissement de ligne d'arrivée par analyse vidéo.

## Schéma du système complet
```
  # Départ

             +-----------+      +------------+
             |           |      |            |
             |        +--+------+---+        |
 +-----------+-----+  |             |  +-----+------+
 | Portique départ |  |   Arduino   |  |Module nRF21|
 |(capteur contact)|  |   (Départ)  |  +------------+
 +-----------------+  |             |
                      +-------------+


+------------------------------------------------------+
  # Arrivée
                           +-------------------+
                           |                   |
                           |   PC monitoring   |
   +-------------+         |   (facultatif)    |
   |Module nRF24 |         |                   |
   +------+------+         +-------------------+
          |                          R|
          |                          J|
          |                          4|
   +------+------+                   5|
   |             |          +------------------+
   |   Arduino   |    USB   |                  |
   |  (Arrivée)  +----------+  Raspberry Pi 4  |
   |             |          |                  |
   +-------------+          +------------------+

```

## Table des matières
1. **[Fonctionnement général](#fonctionnement-général)**
2. **[Prise en main](#prise-en-main)**  
    2.1. [Départ](#départ)  
    2.2. [Arrivée](#arrivée)  
    2.3. [Assemblage](#assemblage)  
    2.4. [Environnement d'exécution](#environnement-dexécution)  
    2.5. [Connexion ssh via cable RJ45](#connexion-ssh-via-cable-rj45)
3. **[Pistes d'amélioration](#pistes-damélioration)**  
	3.1. [Liaison radio](#liaison-radio)  
	3.2. [Traitement des images](#traitement-des-images)  

# Fonctionnement général
Le départ de la course est matérialisé par un portique prenant la forme d'une structure en aluminium avec une tige qui maintient un capteur de fin de course en position fermée. Lors du départ d'un participant, la tige se tourne et relâche le capteur qui est connecté au premier Arduino. Ainsi, nous récupérons état logique d'ouverture du portique. Cet état logique est transmis au deuxième Arduino via une paire d'antennes nRF24. Une fois reçu, l'état logique est transmis via une connexion USB au Raspberry Pi. Un programme en Python est ensuite chargé de garder en mémoire le timestamp au moment du départ puis de le soustraire au timestamp au moment où on détecte un mouvement à l'image obtenue via la pi cam avec la bibliothèque OpenCV.

# Prise en main
## Départ
Pour mettre en place le portique de départ il faut téléverser le code correspondant disponible dans le dossier *arduino* sur un carte Arduino Uno. L'alimentation peut être fournie par une batterie externe. Il faut ensuite réaliser le branchement du module nRF24 en suivant les informations suivantes : 

| Broches module | Broches Arduino |
| :------------: | :-------------: |
| CE             | 9               |
| CSN            | 10              |
| MOSI           | 11              |
| MISO           | 12              |
| SCK            | 13              |
| VCC            | 3.3V            |
| GND            | GND             |

Le capteur de fin de course est à brancher sur la broche 8.
Une fois l'Arduino mis sous tension, il commence à transmettre l'état logique de fermeture du portique.

## Arrivée
Pour l'arrivée il vous faudra un Raspberry avec une distribution GNU/Linux comme [Raspbian](https://www.raspberrypi.org/downloads/), la procédure d'installation est décrite de manière très détaillée dans la documentation officielle.

## Assemblage
Le module nRF24 est connecté de la même façon à l'Arduino que sur le portique de départ, sauf qu'ici le branchement est simple puisqu'un *shield* a été conçu. L'Arduino est alimenté par un port USB du Raspberry Pi. La liaison série passera par ce même câble. La pi cam est branchée au Raspberry Pi via le port dédié à cet effet. Le Raspberry Pi est alimenté par un port USB de l'ordinateur de contrôle. En cas de doute reportez-vous au [`schéma`](#schéma-du-système-complet).

## Environnement d'exécution

Pour exécuter le programme sur le Raspberry Pi, il vous faudra un environnement python avec le package OpenCV, nous vous conseillons de vous renseignez sur l'utilisation de virtualenv.

```
pip3 install opencv # Sur le Pi
```

Ensuite il faut cloner le dépot git du projet :
```
git clone https://git.elukerio.org/mablr/sys_chrono_ski # toujours sur le Pi
```

Puis 
```
cd sys_chrono_ski
```

Il faut rendre exécutables les programmes Python :
```
chmod +x rpi/*.py
```
Vous avez à votre disposition 3 programmes :

- cds.py : programme principal servant pour la course, il suffit de le lancer et de suivre les instructions.
- opcv2.py : permet de tester la caméra et sa senbilité.
- serial.py : permet de tester si les liaisons entres les différentes parties fonctionnent, en affichant l'état d'ouverture du portique. 

## Connexion ssh via cable RJ45
Lors de la course, il faut connecter le Raspberry Pi à votre ordinateur avec un cable RJ45.

Afin de se connecter en SSH, il faut établir une connexion réseau via le cable RJ45. Ainsi sur votre ordinateur (procédure testée sur Debian, Ubuntu et Archlinux), utilisez votre gestionnaire réseau pour configurer une nouvelle connexion filaire. Pour la configuration IPv4, choisissez la méthode "Partagé avec d'autres ordinateurs". Les paramètres restants peuvent être laissés par défaut.

Une fois la connexion ethernet établie entre l'ordinateur et le Raspberry Pi, chacun d'entre eux a une adresse IP. Il va falloir connaitre celle du Raspberry Pi pour pouvoir s'y connecter.

On détermine d'abord l'IP de la machine et le CIDR : (l'adresse doit normalement appartenir au bloc 169.254.0.0/16) 
```
ip addr | grep "inet 169"
```
On obtient ainsi par exemple 169.254.130.1/24

Pour trouver l'adresse du Raspberry Pi, il vous suffira de scanner le réseau avec nmap :
```
nmap -sP 169.254.130.0/24
```
La commande retourne normalement un hôte "Raspberry Pi Foundation" suivit de son adresse IP (par exemple 169.254.130.42).

Il suffit maintenant de se connecter en SSH :
```
ssh pi@169.254.130.42
```
Attention, cette connexion n'est utile que lors de la course, pour réaliser l'installation de l'environnement d'exécution, il faut avoir un accès à internet.

Avec cette connexion SSH, vous pourrez lancer le programme de la course `cds.py`, depuis votre ordinateur. Le principal avantage de cette méthode, est de ne pas nécessiter un écran connecté en HDMI. 

# Pistes d'amélioration
Le code de ce projet est généreusement commenté, vous pourrez donc aisément comprendre son fonctionnement en le lisant. Les améliorations possibles sont nombreuses. Si vous reprenez le code veillez à bien respecter les termes de sa licence (GNU GPL v3).

## Liaison radio
Il serait envisageable d'améliorer le système en optant pour des antennes ayant une meilleure portée.

La sécurité de la transmission radio peut être améliorée. (cf @electroalex26 pour le pentest) 

## Traitement des images
La bibliothèque OpenCV offre des possibilités impressionnantes, vous pouvez laisser parler votre créativité pour inventer de nouvelles fonctionnalités. Voici la [`documentation`](https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_tutorials.html). 
